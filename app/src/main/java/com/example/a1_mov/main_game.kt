package com.example.a1_mov

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Message
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


class main_game : Fragment() {
    internal lateinit var welcomeMessage: TextView
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var goButton: Button
    internal lateinit var timeLeftTextView: TextView
    //internal lateinit var
    // TODO: Rename and change types of parameters
    @State
    var score = 0
    var gameStarted = false

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private lateinit var countDownTimer: CountDownTimer
    @State
    var timeleft = 0


     private val args: WelcomeFRagment by navArgs()

    private var listener: OnFragmentInteractionListener? = null


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        StateSaver.saveInstanceState()
        //outState.putInt(SCORE_KEY,score)

    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //super.onViewCreated(view, savedInstanceState)
        StateSaver.restoreInstanceState(this, savedInstanceState)
        gameScoreTextView = view.findViewById(R.id.score_view)
        goButton = view.findViewById(R.id.gobutton)
        if (savedInstanceState != null) {
            score = savedInstanceState.getInt(SCORE_KEY)
        }
        gameScoreTextView.text = getString( score)

        welcomeMessage = view.findViewById(R.id.welcome_message)
        val playerName = args.playerName
        welcomeMessage.text = getString(
            R.string.welcome
                    playerName.toString()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_game, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }


    companion object {
        private val SCORE_KEY = "SCORE"


        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            main_game().apply {
                arguments = Bundle().apply {

                }
            }
    }


    private fun incrementScore() {
        score++
        gameScoreTextView.text = getString(R.string.score, score)
    }

    private fun resetGame() {
        score = 0
        gameScoreTextView.text = getString(R.string.score, score)
        timeleft = 10
        timeLeftTextView.text = getString(R.string.timeleft, timeleft)
        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            override fun onFinish() {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTick(millisUntilFinished: Long) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

                timeleft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left, timeleft)
            }
        }
        gameStarted = false

    }

    private fun startGame() {
        if (!gameStarted) {
            countDownTimer.start()
            gameStarted = true
        }
    }
}
